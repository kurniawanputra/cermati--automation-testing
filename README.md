Automation Testing Web menggunakan codeceptjs

Ini adalah contoh project automation testing saya menggunakan codeceptjs.
mengapa saya menggunakan menggunakan codeceptjs?
karena codeceptjs dangat familiar dan mudah untuk digunakan.
selain itu codeceptjs sudah terintegrasi dengan :
- Web Driver
- Puppeteer
- Protractor
- Testcafe
- Nightmare
- Appium
- Detox


semua sintaks yang digunakan pada codeceptjs adalah sama, keuntunganya adalah apabila kita sudah terbiasa dengan sintaksnya maka akan mudah kita integrasi ke driver yang lain. ex : web driver(selenium dll)

disini saya mencoba membuat 5 testcase

untuk menjalankan testcasenya cukup ketik :

    yarn start

Semoga ini bermamfaat.

Terima kasih
